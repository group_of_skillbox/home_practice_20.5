// Copyright Epic Games, Inc. All Rights Reserved.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/GameModeBase.h"
#include "Home_Practice_20_5GameModeBase.generated.h"

/**
 * 
 */
UCLASS()
class HOME_PRACTICE_20_5_API AHome_Practice_20_5GameModeBase : public AGameModeBase
{
	GENERATED_BODY()
	
};
